2014-08-11
==========
09:00-09:15 - LCSCMR-1486: Stuff
09:15-03:15 - LCSCMR-1080: Stuff
03:15-04:15 - LCSCMR-1488: Stuff
04:15-05:00 - LCSCMR-1080: Stuff

2014-08-12
==========
09:00-09:15 - Overhead (time, email)
09:15-09:30 - LCSCMR-1486: Stuff
09:30-10:15 - LCSCMR-1487: Stuff
10:15-10:45 - LCSCMR-1486: Stuff
10:45-12:45 - LCSCMR-1081: Stuff
01:45-02:00 - LCSCMR-1488: Stuff
02:00-02:30 - LCSCMR-1082: Stuff
02:30-03:00 - LCSCMR-1083: Stuff
03:00-04:00 - LCSCMR-1486: Stuff
04:00-04:30 - LCSCMR-1083: Stuff

2014-08-13
==========
08:15-08:30 - Overhead (time, email)
08:30-09:30 - LCSCMR-1084: Stuff
09:30-12:00 - LCSCMR-1085: Stuff
12:00-01:00 - Overhead (competency discussion)
01:00-04:30 - LCSCMR-1085: Stuff
