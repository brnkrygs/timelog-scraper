function DateLine( line ){
    var self = this;

    self.line = line;

    return {
        entry_date: self.line
    };
}

module.exports = DateLine;
