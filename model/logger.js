function Logger(){
    var self = this;

    self.current_entry_date = '';
    self.time_log = {};

    function initializeEntryDate( accumulator, entry_date ){
        self.current_entry_date = entry_date;

        if( !accumulator[self.current_entry_date] ){
            accumulator[self.current_entry_date] = { 
                entries:{} 
            };
        }
    }

    function initializeEntryIssue( accumulator, issue_id ){
        if( !accumulator[self.current_entry_date].entries[issue_id] ){
            accumulator[self.current_entry_date].entries[issue_id] = { 
                hours: 0, 
                mins: 0,
                descriptions: []
            };
        }
    }

    function appendIssueDiff( accumulator, issue_id, diff, desc ){
        var issue_entry = accumulator[self.current_entry_date].entries[issue_id];

        issue_entry.hours += diff.hours;
        issue_entry.mins += diff.mins;

        if( desc && issue_entry.descriptions.indexOf( desc ) < 0 ){
            issue_entry.descriptions.push( desc );
        }

        // Roll over minutes
        rollOverMinutes( issue_entry );
    }

    function append( accumulator, parsedLine ){

        if( parsedLine.entry_date ){
            initializeEntryDate( accumulator, parsedLine.entry_date );
        }

        if( parsedLine.entry_issue ){
            initializeEntryIssue( accumulator, parsedLine.entry_issue );
            appendIssueDiff( accumulator, parsedLine.entry_issue, parsedLine.issue_diff, parsedLine.issue_desc );
        }
    }

    function rollOverMinutes( hourMinPair ){
        while( hourMinPair.mins >= 60 ){
            hourMinPair.hours += 1;
            hourMinPair.mins -= 60;
        }
    }

    return {
        append: append,
        rollOverMinutes: rollOverMinutes
    }
}

module.exports = Logger;
