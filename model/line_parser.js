var IssueLine = require( './issue_line' );
var DateLine = require( './date_line' );

function LineParser(){
    var self = this;

    function parse( line ){
        line = line.trim();

        var issue_matches = line.match( /[a-zA-Z]+-\d+/ );
        if( issue_matches ){
            return new IssueLine( line, issue_matches[0] );
        }

        var overhead_matches = line.match( 'Overhead' );
        if( overhead_matches ){
            return new IssueLine( line, overhead_matches[0] );
        }

        var presales_matches = line.match( 'Presales' );
        if( presales_matches ){
            return new IssueLine( line, presales_matches[0] );
        }

        var training_matches = line.match( 'Training' );
        if( training_matches ){
            return new IssueLine( line, training_matches[0] );
        }

        var date_matches = line.match( /^\d\d\d\d-\d\d-\d\d$/ );
        if( date_matches ){
            return new DateLine( line );
        }

        return null;
    }

    return {
        parse: parse
    };
}

module.exports = LineParser;
