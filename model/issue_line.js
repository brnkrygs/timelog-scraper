function IssueLine( line, issue_id ){
    var self = this;

    self.line = line;
    self.issue_id = issue_id;

    self.start_date = null;
    self.end_date = null;

    self.diff = null;
    self.desc = null;

    parseLine( );

    calculateDiff( );

    function calculateDiff( ){
        if( self.start_date === null ){
            throw new Error( 'End date cannot be null when calculating Diff.');
        }

        if( self.end_date === null ){
            throw new Error( 'End date cannot be null when calculating Diff.');
        }

        var diff = self.end_date - self.start_date;

        var millisec = diff;

        // Convert to hours, remove hours from diff amount
        var hh = Math.floor(millisec / 1000 / 60 / 60);
        millisec -= hh * 1000 * 60 * 60;

        // Convert to mins, remove mins from diff amount
        var mm = Math.floor(millisec / 1000 / 60);
        millisec -= mm * 1000 * 60;

        self.diff = {
            hours: hh,
            mins: mm
        };
    }

    function parseLine(){
        var trimmed_line = self.line.trim();
        var line_parts = trimmed_line.split('-');
        var start_time = line_parts[0].split(':');
        var end_time = line_parts[1].split(':');

        var start_date = new Date(2000,0,1, start_time[0], start_time[1] );
        var end_date = new Date(2000,0,1, end_time[0], end_time[1] );

        if( end_date < start_date ){
            end_date.setHours( end_date.getHours() + 12 );
        }

        self.start_date = start_date;
        self.end_date = end_date;
        var line_parts_colon = trimmed_line.split(':');
        self.desc = line_parts_colon[line_parts_colon.length-1].trim();
    }

    return {
        entry_issue: self.issue_id,
        issue_diff: self.diff,
        issue_desc: self.desc
    };
}

module.exports = IssueLine;
