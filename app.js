var fs = require('fs');
var LineParser = require('./model/line_parser.js');
var Logger = require('./model/logger.js');

var parser = new LineParser();
var logger = new Logger();

function buildTotal( time_entries ){

    Object.keys(time_entries).reduce( function( prev, curr ){

        time_entries[curr].total = {
            hours: 0,
            mins:0
        };

        Object.keys( time_entries[curr].entries ).reduce( function( eprev, entry_key ){

            var entry = time_entries[curr].entries[entry_key];

            time_entries[curr].total.hours += entry.hours;
            time_entries[curr].total.mins += entry.mins;

            logger.rollOverMinutes( time_entries[curr].total );

        }, time_entries[curr].total );

    }, time_entries );
}

function displayEntries( entries ){

    var grandTotal = { hours: 0, mins:0 };
    var line = '';

    Object.keys( entries ).forEach( function( day_key ){

        var entry = entries[day_key];
        line += "== " + day_key + "\n";
        line += "=== Issues:\n";

        Object.keys( entry.entries ).forEach( function( issue_id ){
            var issue = entry.entries[issue_id];
            var dec = formatDiffAsDecimal( issue );
            line += issue_id + ": (hours: "+ issue.hours+ "mins: "+ issue.mins + "dec: "+dec + ")\n";

            issue.descriptions.forEach( function( d ){
                line += " * " + d + "\n";
            });
        });

        var dec = formatDiffAsDecimal( entry.total );
        grandTotal.hours += entry.total.hours;
        grandTotal.mins += entry.total.mins;
        line += "=== Total:\n";
        line +=  "Hours: " + entry.total.hours + " Mins: " + entry.total.mins+ "("+dec+")\n\n";
    });

    line += "\n";
    line += "== Grand Total\n";
    logger.rollOverMinutes( grandTotal );
    line += "Hours: " + grandTotal.hours + " Mins: " + grandTotal.mins + "\n";

    console.log( line );
    fs.writeFile( "timelog-out.markdown", line );

}

function formatDiffAsDecimal( diff ){
    var result = diff.hours.toString();

    var mins = diff.mins / 60;
    mins = mins.toFixed(2).toString();
    result += '.' + mins.split('.')[1];

    return result;
}

fs.readFile( './time-log.markdown', 'utf8',  
    function( err, data ){
        if( err ){
            return console.log( err );
        }

        var time_entries = {};

        var lines = data.trim().split('\n');

        // For each line, check for an issue ID
        //  If issue ID found, get the time start and time end
        //  Math all the things
        //  Stash them

        lines.reduce( function( prev, curr ){

            curr = curr.trim();

            var parsed = parser.parse( curr );

            if( parsed != null ){
                logger.append( time_entries, parsed );
            }

            return time_entries;

        }, time_entries );

        buildTotal( time_entries );

        displayEntries( time_entries );
    }
);
